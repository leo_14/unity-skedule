﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using LitJson;

public class JSONTask {

	public string Description { get; set; }
	public string CreatedDateTime { get; set; }
	public string CompletedDateTime { get; set; }
	public string DueDate { get; set; }
	public string Tags { get; set; }
	public string Urgent { get; set; }
}

public class TaskManager : ScenarioManager
{

	// methods to add and remove tasks from json file



	// object to populate checklist
	public Transform taskContentTransform;

	//public GameObject addPanel;
	//panelObjectDictionary["NewTaskPanel"]

	public Button taskAddButton;

	//public InputField[] addInputFields;

	public GameObject taskItemPrefab;

	public InputField taskDescriptionInputField;
	public InputField taskDueDateInputField;
	public InputField taskTagsInputField;
	public Toggle taskUrgentToggle;





	static public List<GameObject> taskObjects;

	private void Awake() {

		taskObjects = new List<GameObject>();
		//addInputFields = panelObjectDictionary["NewTaskPanel"].GetComponentsInChildren<InputField>();






	}





	public void NewTaskPanelButton() {
		panelObjectDictionary["NewTaskPanel"].SetActive(true);

	}

	public void AddNewTaskButton() {

		CreateTaskObject(taskDescriptionInputField.text, System.DateTime.Now.ToString(),null, taskDueDateInputField.text, taskTagsInputField.text, taskUrgentToggle.isOn);


		ClearTaskInput();
		panelObjectDictionary["NewTaskPanel"].SetActive(false);
	}

	public void DeleteNewTaskButton() {
		ClearTaskInput();
		panelObjectDictionary["NewTaskPanel"].SetActive(false);
	}


	void ClearTaskInput() {
		taskDescriptionInputField.text = "";
		taskDueDateInputField.text = "";
		taskUrgentToggle.isOn = false;
	}



	public void IterateThroughTaskObjectList() {
		Debug.Log("==============================");
		Debug.Log("List of Tasks in tasksObjects List.");
		foreach(GameObject taskObject in taskObjects) {
			TaskObjectScript taskObjectScript = taskObject.GetComponent<TaskObjectScript>();
			Debug.Log(taskObjectScript.taskDescriptionText);
		}
		Debug.Log("==============================");
	}

	/*
	public string[] TaskTagsStringToArray(string _tagString) {

		string[] _returnStringArray;

		if(_tagString == "" || _tagString == null) {
			Debug.LogWarning("TaskTagsStringToArray was given an empty string , returning empty string array.");
			_returnStringArray = new string[0];
			return _returnStringArray;
		} else {
			_returnStringArray = _tagString.Split(',');
		}

		Debug.Log("_returnStringArray: " + _returnStringArray);
		return _returnStringArray;

	}
	public string TaskTagsArrayToString(string[] _tagStringArray) {

		string _returnString = "";

		if (_tagStringArray == null || _tagStringArray.Length == 0) {
			Debug.LogWarning("TaskTagsArrayToString was given an empty string array, returning empty string.");
			return _returnString;
		}
		for(int i = 0; i < _tagStringArray.Length; i++) {
			if(i == _tagStringArray.Length - 1) {
				_returnString += _tagStringArray[i];
			} else {
				_returnString += _tagStringArray[i] + ",";
			}
		}
		Debug.Log("_returnString: " + _returnString);
		return _returnString;
	}
	*/
	public string ReturnNewGameObjectNameWithSuffix(string _name, string _suffix) {
		string _returnString = "";

		string[] _nameArray = _name.Split(' ');

		for(int i = 0; i < _nameArray.Length; i++) {
			_returnString += _nameArray[i];
		}
		_returnString += _suffix;

		return _returnString;
	}


	void CreateTaskObject(
		string _taskDescriptionText,
		string _taskCreatedDateText,
		string _taskCompletedDateTimeText,
		string _taskDueDateText,
		string _taskTagsText,
		bool _taskUrgent
		) {

		GameObject taskObject = Instantiate(taskItemPrefab);

		taskObject.name = ReturnNewGameObjectNameWithSuffix(_taskDescriptionText, "_Task");
		taskObject.transform.SetParent(taskContentTransform);
		TaskObjectScript taskObjectScript = taskObject.GetComponent<TaskObjectScript>();


		taskObjectScript.InitialiseTaskObject(_taskDescriptionText,_taskCreatedDateText,_taskCompletedDateTimeText, _taskDueDateText, _taskTagsText,_taskUrgent);


		Debug.Log("Adding task object: " + taskObject.name);
		taskObjects.Add(taskObject);
		Debug.Log("taskObjects Count: " +taskObjects.Count);

	}

	public void CompleteTask(GameObject taskObject) {
		//taskObjects.Remove(taskObject);
		TaskObjectScript taskObjectScript = taskObject.GetComponent<TaskObjectScript>();

		// if task isnt complete - add todays date
		if(taskObjectScript.taskCompletedDateTimeText == null || taskObjectScript.taskCompletedDateTimeText == "") {

			taskObjectScript.taskCompletedDateTimeText = System.DateTime.Now.ToString();

			// remove priority colour
			taskObjectScript.checkButtonColourImage.color = Color.grey;
		}
		// if task is complete - remove todays date
		else {

			//add warning message?

			taskObjectScript.taskCompletedDateTimeText = null;

			// add colour back based on priority
			if(taskObjectScript.taskPriority <= 2) {
				taskObjectScript.checkButtonColourImage.color = Color.red;
			} else if(taskObjectScript.taskPriority <= 4) {
				taskObjectScript.checkButtonColourImage.color = Color.yellow;
			} else {
				taskObjectScript.checkButtonColourImage.color = Color.green;
			}

		}
	}

	public void EditTask(GameObject taskObject) {

		//TaskObjectScript taskObjectScript = taskObject.GetComponent<TaskObjectScript>();

		//show edit task window

	}


	public bool IsTaskCurrentlyCancelled(TaskObjectScript taskObjectScript) {


		return true;
	}


	public void CancelTask(GameObject taskObject) {

		TaskObjectScript taskObjectScript = taskObject.GetComponent<TaskObjectScript>();


		if (IsTaskCurrentlyCancelled(taskObjectScript) == true) {
			//remove cancelled

			// remove complete date
		} else {
			
			// add cancelled


			// add complete date
		}


		// if task isnt complete - add todays date
		if(taskObjectScript.taskCompletedDateTimeText == null|| taskObjectScript.taskCompletedDateTimeText == "") {

			taskObjectScript.taskCompletedDateTimeText = System.DateTime.Now.ToString();

			// remove priority colour
			taskObjectScript.checkButtonColourImage.color = Color.grey;
		}
		// if task is complete - remove todays date
		else {

			//add warning message?

			taskObjectScript.taskCompletedDateTimeText = null;

			// add colour back based on priority
			if(taskObjectScript.taskPriority <= 2) {
				taskObjectScript.checkButtonColourImage.color = Color.red;
			} else if(taskObjectScript.taskPriority <= 4) {
				taskObjectScript.checkButtonColourImage.color = Color.yellow;
			} else {
				taskObjectScript.checkButtonColourImage.color = Color.green;
			}

		}


	}



	//public Queue<string> sentenceQueue;
	string jsonString;
	JsonData taskJsonData;

	string jsonTasksFilePath;

	// tasks is a todo list - not to be used retrospectively
	// calendar is for work/social events
	// skedule is for personal work/habits (will display calendar events but cannot be editted)


	public void ReadTasksFromJSON() {

		//delete all tasks from contents

		taskObjects.Clear();


		//GameObject[] contentChildrenArray = content.GetChild
		//content.GetComponentsInChildren<GameObject>();
		for(int i = 0; i < taskContentTransform.childCount; i++) {
			Destroy(taskContentTransform.GetChild(i).gameObject);
		}




		//check if file exists

		//if no file create new file
		//can return as not data needs to be read

		jsonTasksFilePath = Application.persistentDataPath + "/tasks.json";

		if(!File.Exists(jsonTasksFilePath)) {
			Debug.LogWarning("tasks.json doesn't exist at path: " + jsonTasksFilePath);
			return;
		}

		jsonString = File.ReadAllText(jsonTasksFilePath);

		taskJsonData = JsonMapper.ToObject(jsonString);


		for(int i = 0; i < taskJsonData.Count; i++) {

			CreateTaskObject(
				taskJsonData[i]["Description"].ToString(),
				taskJsonData[i]["DateCreated"].ToString(),
				taskJsonData[i]["DateCompleted"].ToString(),
				taskJsonData[i]["DateDue"].ToString(),
				taskJsonData[i]["Tags"].ToString(),
				StringToBool(taskJsonData[i]["Urgent"].ToString())
				);
				
	
		}

	}


	public bool StringToBool(string _string) {
		if(_string == "true") {
			return true;
		} else if(_string == "false") {
			return false;
		} else {
			Debug.LogWarning("StringToBool doesn't recognise '" + _string + "', returning false.");
			return false;
		}

	}
	public string BoolToString(bool _bool) {
		if(_bool == true) {
			return "true";
		} else if(_bool == false) {
			return "false";
		} else {
			Debug.LogWarning("BoolToString doesn't recognise '" + _bool + "', returning false.");
			return "false";
		}

	}

	public void SaveTasksToJSON() {

		jsonTasksFilePath = Application.persistentDataPath + "/tasks.json";

		if(!File.Exists(jsonTasksFilePath)) {
			Debug.LogWarning("tasks.json doesn't exist at path: " + jsonTasksFilePath);
		}



		Debug.Log("taskObjects count: " + taskObjects.Count);



		Dictionary<string, JSONTask> JSONTaskDictionary = new Dictionary<string, JSONTask>();

		for(int i = 0; i < taskObjects.Count; i++) {

		
			JSONTask _task = new JSONTask();


			TaskObjectScript taskObjectScript = taskObjects[i].GetComponent<TaskObjectScript>();

			_task.Description = taskObjectScript.taskDescriptionText;
			_task.CreatedDateTime = taskObjectScript.taskCreatedDateTimeText;
			_task.CompletedDateTime = taskObjectScript.taskCompletedDateTimeText;
			_task.DueDate = taskObjectScript.taskDueDateText;
			_task.Urgent = BoolToString(taskObjectScript.taskUrgent);

			//_task.Description = taskObjects[i].taskDescriptionText;
			//_task.CreatedDateTime = taskObjects[i].taskCreatedDateTimeText;
			//_task.CompletedDateTime = taskObjects[i].taskCompletedDateTimeText;
			//_task.DueDate = taskObjects[i].taskDueDateText;
			//_task.Tags = taskObjects[i].taskTagsText;
			//_task.Urgent = BoolToString(taskObjects[i].taskUrgent);

			Debug.Log("added task " +_task.Description);

		}
		string jsonTaskString = JsonMapper.ToJson(JSONTaskDictionary);

		Debug.Log("JSONTaskDictionary.Count: " + JSONTaskDictionary.Count);
		
		Debug.Log("taskObjects.Count: " + taskObjects.Count);
		
		Debug.Log("jsonTaskString: " + jsonTaskString);




	}



	void ApplyTaskFilter() {


		//order based on priority
		// order based on date created

		//filter tags

	}
} 

