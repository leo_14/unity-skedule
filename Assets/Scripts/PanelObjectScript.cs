﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelObjectScript : PanelManager {

	[SerializeField]
	bool hideOnInitiation = true;

	void Start() {
		InitialisePanel();
	}

	public void InitialisePanel() {
		/*
		try {
			panelObjectDictionary.Add(this.gameObject.name, this.gameObject);
			Debug.Log("Added Object to PanelDictionary: " + gameObject.name);
		} catch {
			Debug.LogWarning("PanelObjectDictionary: Couldn't add panel '" + gameObject.name + "'.");
		}*/
		panelObjectDictionary.Add(this.gameObject.name, this.gameObject);
		Debug.Log("Added Object to PanelDictionary: " + gameObject.name);

		if(hideOnInitiation) {
			try {
				Debug.Log("PanelObject: " + this.gameObject.name + " hidden successfully.");
				this.gameObject.SetActive(false);
			} catch {
				Debug.Log("PanelObject: " + this.gameObject.name + " cannot be hidden (can't find canvaseRenderer).");
			}
		}
	}
}


