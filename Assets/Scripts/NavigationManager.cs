﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationManager : ScenarioManager {


	//bool to check initiation panel
	bool clickedNavigationToStart = false;

	public void Navigation_Button(GameObject panelToNavigateTo) {

		if(!clickedNavigationToStart) {

		
				Destroy(panelObjectDictionary["IntroductionPanel"]);
				panelObjectDictionary["ClearPanel"].gameObject.SetActive(false);
	
		}

		if(currentPanel != null) {
			currentPanel.gameObject.SetActive(false);
		}
		currentPanel = panelToNavigateTo;
		currentPanel.gameObject.SetActive(true);


	}




}
