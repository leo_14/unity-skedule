﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LitJson;

public class TaskObjectScript : TaskManager {


	[Header("Task Objects")]
	public GameObject checkButton;
	public Image checkButtonColourImage;
	//public GameObject checkButtonCheckImage;
	//public GameObject checkButtonUndoImage;


	public Text taskDescriptionTextObject;
	public Text taskDueDateTextObject;
	public Text taskTagsTextObject;


	[Header("Task Data")]


	//public int jsonIndex;
	//public int listIndex;

	public string taskDescriptionText = null;
	public string taskCreatedDateTimeText = null;
	public string taskCompletedDateTimeText = null;
	public string taskDueDateText = null;
	public string taskTagsText = null;
	public bool taskUrgent = false;
	public float taskPriority = 6;  // (green: 6 5) (yellow: 4 3) (red: 2 1)
	

	[Header("Misc Vars")]

	Color transparentColour;


	void Start() {
		// hide undo task completion image
		transparentColour = new Color(0, 0, 0, 0);
		//checkButtonUndoImage.SetActive(false);
	}





	//When new task made, initialise here
	public void InitialiseTaskObject(
		string _taskDescriptionText,
		string _taskCreatedDateText,
		string _taskCompletedDateTimeText,
		string _taskDueDateText,
		string _taskTagsText,
		bool _taskUrgent
		) {


		//taskObjects.Add(this.gameObject);

		taskDescriptionText = _taskDescriptionText;
		taskCreatedDateTimeText = _taskCreatedDateText;
		taskCompletedDateTimeText = _taskCompletedDateTimeText;
		taskDueDateText = _taskDueDateText;
		taskTagsText = _taskTagsText;
		taskUrgent = _taskUrgent;



		//priority algorithm

		taskPriority = 6;

		if (taskDueDateText == null || taskDueDateText == "") {
			// do nothing
		} else {
		 
			System.DateTime taskDueSystemDateTime;
			try {
				string[] taskDueDateSplitText = taskDueDateText.Split('-');
				//System.DateTime taskDueSystemDateTime = new System.DateTime(
				taskDueSystemDateTime = new System.DateTime(
					int.Parse(taskDueDateSplitText[0]),
					int.Parse(taskDueDateSplitText[1]),
					int.Parse(taskDueDateSplitText[2])
					);
			} catch {
				Debug.LogWarning("Error parsing DateTime");
				taskDueSystemDateTime = new System.DateTime(2030, 02, 16);
			}
			System.DateTime todaySystemDateTime = System.DateTime.Now;
			double daysUntilTaskDue = (todaySystemDateTime - taskDueSystemDateTime).TotalDays;


			if (daysUntilTaskDue <= 2) {
				taskPriority = 2;
			}
			else if(daysUntilTaskDue <= 7) {
				taskPriority = 4;
			} else {
				taskPriority = 6;
			}

		}





		// add cancel task to init















		if(taskUrgent==true) {
			taskPriority = taskPriority / 2;
		}

		if(taskPriority <= 2) {
			checkButtonColourImage.color = Color.red;
		} else if(taskPriority <= 4) {
			checkButtonColourImage.color = Color.yellow;
		} else {
			checkButtonColourImage.color = Color.green;
		}
		//Debug.Log(this.gameObject.name + " task priority: " + taskPriority);

		taskDescriptionTextObject.text = taskDescriptionText;
		taskDueDateTextObject.text = taskDueDateText;
		taskTagsTextObject.text = taskTagsText;




	}

}
